using Microsoft.AspNetCore.Mvc;

namespace pruebagit.Controllers;

[ApiController]
[Route("[controller]")]
public class GrafoController : ControllerBase
{
    [HttpGet(Name = "EnOrden")]
    public string EnOrden()
    {
        nodo raiz = new nodo(8);
        raiz.izq = new nodo(3);
        raiz.izq.izq = new nodo(1);
        raiz.izq.der = new nodo(6);
        raiz.izq.der.izq = new nodo(4);
        raiz.izq.der.der = new nodo(7);
        raiz.der = new nodo(10);
        raiz.der.der = new nodo(14);
        raiz.der.der.izq = new nodo(13);
        recorrer r = new recorrer();
        string resultado = r.RecorrerEnOrden(raiz);

        return resultado.Substring(0, resultado.Length - 2);
    }
}

public class recorrer {
    string resultado = "";
    public string RecorrerEnOrden(nodo x) {
        if (x.izq != null)
            RecorrerEnOrden(x.izq);
        resultado += x.valor + ", ";
        if (x.der != null)
            RecorrerEnOrden(x.der);
        return resultado;
    }
}

public class nodo {
    public int valor {get;set;}
    public nodo izq {get;set;}
    public nodo der {get;set;}

    public nodo(int valor) {
        this.valor = valor;
    }
}