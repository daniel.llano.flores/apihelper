using Microsoft.AspNetCore.Mvc;

namespace pruebagit.Controllers;

[ApiController]
[Route("[controller]")]
public class FechaController : ControllerBase
{


    [HttpGet(Name = "CalcularEdad")]
    public int CalcularEdad(DateTime date)
    {
        DateTime hoy = DateTime.Now;
        int edad=hoy.Year-date.Year;
        if (hoy.Month < date.Month || (hoy.Month == date.Month && hoy.Day < date.Day))        
            edad --;
        
        return edad;
    }
}
