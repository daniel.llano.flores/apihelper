using Microsoft.AspNetCore.Mvc;

namespace pruebagit.Controllers;

[ApiController]
[Route("[controller]")]
public class CalculadoraController : ControllerBase
{
    [HttpPost("Suma")]
    public string Suma(float n,float m)
    {
        return "La Suma es :"+(n+m);
    }

    [HttpGet("Resta")]
    public int Resta(int a, int b)
    {
        return a-b;
    }

    [HttpGet("DivisionDecimal")]
    public float DivisionDecimal(float a, float b)
    {   float resultado = 0;
        if(b !=0)
           resultado = a/b;
        return resultado;
    }
    
    [HttpPost("Division")]
    public string division(int valor1, int valor2)
    {
        int resultado =  valor1/valor2;
        int residuo =  valor1%valor2;
        if(residuo == 0)
            return "Resultado: " + resultado;
        else
            return "El resultado entero es " + resultado + ", y tiene residuo: " + residuo;
    }

    [HttpPost]
    [Route("Multiplicacion")]
    public string Multiplicacion(int num1, int num2)
    {
        return "La multiplicacion es:" + (num1*num2);
    }
}
